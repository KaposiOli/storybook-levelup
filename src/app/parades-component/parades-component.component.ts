import { Component, Input, Output } from '@angular/core';
import { Kenobi } from './event';

@Component({
  selector: 'app-parades-component',
  templateUrl: './parades-component.component.html',
  styleUrls: ['./parades-component.component.scss']
})
export class ParadesComponentComponent {
  constructor() {}
  @Input() General: Kenobi = {
    isABoldOne: '',
    hasLightSaber: '',
    isExecutedByHisOwnClonesDuringOrder66: ''
  };
  @Output()
  onClick() {
    this.General.isABoldOne = 'General Kenobi! I see you are a bold one!';
    this.General.hasLightSaber = 'I see you have lost your lightsaber!';
    this.General.isExecutedByHisOwnClonesDuringOrder66 =
      'No one could have survived that fall.';
  }
}
