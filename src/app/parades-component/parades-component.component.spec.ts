import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParadesComponentComponent } from './parades-component.component';

describe('ParadesComponentComponent', () => {
  let component: ParadesComponentComponent;
  let fixture: ComponentFixture<ParadesComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParadesComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParadesComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
