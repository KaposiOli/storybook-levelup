export interface Kenobi {
  isABoldOne: string;
  hasLightSaber: string;
  isExecutedByHisOwnClonesDuringOrder66: string;
}
