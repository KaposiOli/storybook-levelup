import { action } from '@storybook/addon-actions';
import { ParadesComponentComponent } from 'src/app/parades-component/parades-component.component';

export default {
  title: 'Memeness',
  component: ParadesComponentComponent
};

export const order66 = () => ({
  component: ParadesComponentComponent
});

order66.story = {
  name: 'What do we do Master?',
  parameters: { notes: 'Palpatine is the Sith lord we have been looking for!' }
};
